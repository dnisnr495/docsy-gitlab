---
title: Shortcodes
---

> Note: You can edit this page in the [`docsy-gitlab` theme project](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab/-/tree/main/content/docs?ref_type=heads).

Shortcodes are pre-defined ways to display content. The shortcodes are listed alphabetically after the shortcode explanatory sections.

<!-- markdownlint-disable MD037 -->
## Using shortcodes

There are two ways to call shortcodes:

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="md" >}}
{{%/* shortcodename parameters */%}}` with markdown rendering
{{</* shortcodename parameters */>}} without markdown rendering
{{< /card >}}
{{< /cardpane >}}

More information can be found in the [Hugo shortcodes documentation](https://gohugo.io/content-management/shortcodes/#use-shortcodes).

## Hugo and Docsy shortcodes

The Hugo framework has a few shortcodes included, with a list on the [Hugo shortcodes documentation](https://gohugo.io/content-management/shortcodes/#use-shortcodes). These include:

1. Figure (instead of using HTML)
1. YouTube embed

The base Docsy theme also has a number of shortcodes, which you can find in [the Docsy shortcodes documentation](https://www.docsy.dev/docs/adding-content/shortcodes/).

Available shortcodes include:

1. Info box
1. Card pane
1. Include content from another file

## Alerts

{{< note >}}
This shortcode is part of the Docsy base theme, not a custom shortcode.
{{< /note >}}

The `color` can be specified, commonly with `warning` (orange), or `danger` (red).

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="md" >}}This is a regular paragraph.

{{%/* alert title="Note" color="primary" */%}}
This is an out of context note that you want to draw a users attention to.
{{%/* /alert */%}}
{{< /card >}}
{{< card header="**Output**" >}}
<p>This is a regular paragraph.</p>
<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Note</h4>
<p>This is an out of context note that you want to draw a users attention to.</p>
</div>
{{< /card >}}
{{< /cardpane >}}

## Comments

These comments are turned into markdown comments.
They are only viewable when viewing the pages source code and are removed before the page is rendered.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="mkd" >}}This is a paragraph.
{{</* comment */>}}
This is a comment which is
completely ignored.
{{</* /comment */>}}
Next paragraph is here.
{{< /card >}}
{{< card header="**Output**" >}}
This is a paragraph.
{{< comment >}}
This is a comment which is
completely ignored.
{{< /comment >}}
Next paragraph is here.
{{< /card >}}
{{< card code=true  header="**Output HTML**" lang="html">}}<p>This is a paragraph.</p>
<p>Next paragraph is here.</p>
{{< /card >}}
{{< /cardpane >}}

## Labels

| Attribute          | Description                                                 |
| ------------------ | ----------------------------------------------------------- |
| `name` (required)  | Name for the label. Scoped labels are automatically handled |
| `color` (optional) | Hex color code for the label, defaults to `#428BCA`         |
| `light` (optional) | Only required for light colors to make the label text dark  |

### Unscoped labels

```md
{{</* label name="Category:Container Registry" */>}}
{{</* label name="default-priority" color="#FFECDB" light="true" */>}}
```

{{< label name="Category:Container Registry" >}}
{{< label name="default-priority" color="#FFECDB" light="true" >}}

### Scoped labels

```md
{{</* label name="priority::1" color="#cc0000" */>}}
{{</* label name="Department::Product Security Engineering" color="#69d100" light="true" */>}}
```

{{< label name="priority::1" color="#cc0000" >}}
{{< label name="Department::Product Security Engineering" color="#69d100" light="true" >}}

## Notes

The classic style notes are smaller and can be used to mention something out of context.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="md" >}}This is a regular paragraph.

{{</* note */>}}
a note is something that needs to be mentioned but is apart from the context.
{{</* /note */>}}
{{< /card >}}
{{< card header="**Output**" >}}
This is a regular paragraph.
{{< note >}}
A note is something that needs to be mentioned but is apart from the context.
{{< /note >}}
{{< /card >}}
{{< /cardpane >}}

## Panels

To add notes and warning blocks into colorful boxes we have provided a shortcode for a panel.
This is similar to the docsy card shortcode but takes up the whole width of the content area
and can make use of custom header, body, and footer colors.

Use panels when your description contains more than one paragraph, or a
long paragraph. For single and short paragraphs, use alert boxes instead.

Copy paste the following code according to what you want to present to the user
and replace only the description.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="mkd" >}}{{</* panel header="**Note**" header-bg="blue" */>}}
NOTE DESCRIPTION
{{</* /panel */>}}
{{< /card >}}
{{% card header="**Output**" %}}
<div class="card mb-4">
    <div class="card-header -bg-blue">
        <strong>Note</strong>
    </div>
  <div class="card-body">
      <p class="card-text">
          NOTE DESCRIPTION
      </p>
  </div>
</div>
{{% /card %}}
{{< /cardpane >}}

The available colors are any of the named colors below:

<i class="fa-solid fa-square -text-blue"></i> `blue`
<i class="fa-solid fa-square -text-indigo"></i> `indigo`
<i class="fa-solid fa-square -text-purple"></i> `purple`

<i class="fa-solid fa-square -text-pink"></i> `pink`
<i class="fa-solid fa-square -text-red"></i> `red`
<i class="fa-solid fa-square -text-orange"></i> `orange`

<i class="fa-solid fa-square -text-yellow"></i> `yellow`
<i class="fa-solid fa-square -text-green"></i> `green`
<i class="fa-solid fa-square -text-teal"></i> `teal`

<i class="fa-solid fa-square -text-cyan"></i> `cyan`
<i class="fa-solid fa-square -text-gray"></i> `gray`
<i class="fa-solid fa-square -text-black"></i> `black`

<i class="fa-solid fa-square -text-primary"></i> `primary`
<i class="fa-solid fa-square -text-secondary"></i> `secondary`
<i class="fa-solid fa-square -text-success"></i> `success`

<i class="fa-solid fa-square -text-info"></i> `info`
<i class="fa-solid fa-square -text-danger"></i> `danger`
<i class="fa-solid fa-square -text-warning"></i> `warning`

<i class="fa-solid fa-square -text-light"></i> `light`
<i class="fa-solid fa-square -text-dark"></i> `dark`

## Performance Indicator Embeds

**Examples**

### KPI Summary

```md
{{</* performance-indicators/summary org="example" is_key=true /*/>}}
```

{{< performance-indicators/summary org="pi_example" is_key=true >}}

### Regular PI Summary

```md
{{</* performance-indicators/summary org="example" is_key=false /*/>}}
```

{{< performance-indicators/summary org="pi_example" is_key=false >}}

### Key Performance Indicators

```md
{{</* performance-indicators/list org="example" is_key=true /*/>}}
```

{{< performance-indicators/list org="pi_example" is_key=true >}}


### Regular Performance Indicators

```md
{{</* performance-indicators/list org="example" is_key=false /*/>}}
```

{{< performance-indicators/list org="pi_example" is_key=false >}}

## Tableau Embeds

### `tableau`

**Note:** Any other named parameters are added as additional attributes on the `<tableau-viz>` element.

- `src`: URL of visualization to embed
- `height`: default: `400px`
- `toolbar`: `visible | hidden`, default: `hidden`
- `hide-tabs`: `true | false`, default: `false`

**Examples**

```md
{{</* tableau "https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" /*/>}}
```

{{< tableau "https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" />}}

```md
{{</* tableau height="600px" toolbar="visible" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" */>}}
  {{</* tableau/filters "Subtype Label"="bug::vulnerability" /*/>}}
  {{</* tableau/params "Severity Select"="S2" /*/>}}
{{</* /tableau */>}}
```

<!-- markdownlint-disable-next-line MD013 -->
{{< tableau height="600px" toolbar="visible" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" >}}
  {{< tableau/filters "Subtype Label"="bug::vulnerability" >}}
  {{< tableau/params "Severity Select"="S2" >}}
{{< /tableau >}}

```md
{{</* tableau src="https://10az.online.tableau.com/t/gitlab/views/OKR4_7EngKPITest/PastDueInfraDevIssues" */>}}
  {{</* tableau/filters "Subtype Label"="bug::vulnerability" /*/>}}
{{</* /tableau */>}}
```

<!-- markdownlint-disable-next-line MD013 -->
{{< tableau src="https://10az.online.tableau.com/t/gitlab/views/OKR4_7EngKPITest/PastDueInfraDevIssues" >}}
  {{< tableau/filters "Subtype Label"="bug::vulnerability" >}}
{{< /tableau >}}


### `tableau/filters`

Renders a `viz-filter` element for each of the key/value pairs passed as parameters.

```md
{{</* tableau/filters filter1="value1" filter2="value2" */>}}
```

```html
<viz-filter field="filter1" value="value1"></viz-filter>
<viz-filter field="filter2" value="value2"></viz-filter>
```

### `tableau/params`

Renders a `viz-parameter` element for each of the key/value pairs passed as parameters.

```md
{{</* tableau/params param1="value1" param2="value2" */>}}
```

```html
<viz-parameter name="param1" value="value1"></viz-parameter>
<viz-parameter name="param2" value="value2"></viz-parameter>
```
<!-- markdownlint-enable MD037 -->
