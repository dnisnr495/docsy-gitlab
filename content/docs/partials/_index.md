---
title: Partials
---

> Note: You can edit this page in the [`docsy-gitlab` theme project](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab/-/tree/main/content/docs?ref_type=heads).

### `tableau-with-filters`

This partial is the primary way for embedding Tableau charts in the handbooks.
It should be used in all shortcodes that need to embed a tableau chart.
It is set up to a named parameters or a data file as the source.
In each shortcode this partial is called a flag must be set, `hastableau=true`,
so that the correct JavaScript is applied to the page at the time of construction.

- parameters
  - `url`: (required)
    - The shareable URL of the view to be embedded
  - `tabs`: (optional defaults to false)
    - A flag to indicate if other views should be shown as tabs, `true | false`
  - `toolbar`: (optional defaults to hidden)
    - A value to indicate the location, including hidden, of the tool bar as part of the embed. `hidden | top | bottom`
  - `height`: (optional defaults to 300px)
    - The fixed height of the embedded view in pixels.
  - `filters`: (optional)
    - A list of filters and values (key value pairs) to filter the view by.
    When passing the chart data directly filter fields and their values should be separated by an equal symbol, `=`,
    and subsequent fields should be separated with a double pipe symbol, `||`,
    and if multiple values are used in a single filter the values should be separated by a comma symbol `,`.
  - `parameters`: (optional)
    - A list of parameters and values (key value pairs) to set in the view.
    When passing the chart data directly parameter names and their values should be separated by an equal symbol, `=`,
    and subsequent parameters should be separated with a double pipe symbol, `||`.

When referencing the chart data directly

```go
{{ .Page.Store.Set "hastableau" true -}}

// shortcode content 

{{ partial "tableau-with-filters" url="url" tabs="true" toolbar="top" height="200px"   filters="field=value||field=value,value,value" parameters="name=value||name=value" }}
```

When using a data file for the chart data.

```go
{{ .Page.Store.Set "hastableau" true -}}

// shortcode content that gets chart information from a data file

{{- with .tableau_data }}
{{- range .charts }}
{{- partial "tableau-with-filters" . }}
{{- end }}
{{- end }}
```
