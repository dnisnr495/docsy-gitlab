function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

async function checkForPageChanges() {
  try {
    let response = await fetch("/sitemap.xml", {cache: "no-store"});
    let data = await response.text();
    if(data == null || data.length == 0)
      throw("Sitemap XML Data empty.");
    processXml(data);
  } catch (err) {
    console.log("Error getting sitemap from server.  You might need to refresh the page.")
    stopNotifications();
    stopPageUpdates();
    const n = {
      "id": crypto.randomUUID(),
      "type": "info",
      "icon": "fa-solid fa-circle-info",
      "title": "Notifications stopped.",
      "messageHTML": "<p>Notifications have stopped.  This happens if the server responded with no data or an error.  Refresh the page to resume notifications.</p>",
      "autohide": true
    };
    renderNotification(n, false);
    return
  }
}

function processXml(xml) {
  const siteMapDoc = parser.parseFromString(xml,"text/xml");
  const pages = siteMapDoc.getElementsByTagName("url");
  urls = [];
  Array.prototype.slice.call(pages).forEach(e => {
    // This happens in local dev where there's no git information
    // for the current file so no lastmod element.
    if(e.getElementsByTagName("lastmod").length === 0) {
      let lastmod = document.createElement("lastmod");
      let now = new Date()
      lastmod.textContent = arrivalTime.toISOString();
      e.appendChild(lastmod);
    }
    urls.push({
      "url": e.getElementsByTagName("loc")[0].textContent,
      "lastmod": e.getElementsByTagName("lastmod")[0].textContent
    });
  });
  page = urls.find(e => e.url == window.location.href.replace(window.location.search, ""));

  const lastmod = new Date(Date.parse(page.lastmod));

  if(lastmod > arrivalTime)
    renderNotification({
        "id": crypto.randomUUID(),
        "type": "info",
        "icon": "fa-solid fa-circle-info",
        "title": "Page has been update",
        "messageHTML": "<p>This page has been updated on the server.  If you want to see the lastest version please refresh the page.</p>",
        "autohide": false
      }, false);
}

async function processNotifications() {
  let response = null;
  let jsonData = null;
  try {
    response = await fetch('/notifications.json', {cache: "no-store"});
    jsonData = await response.json();
    if(jsonData == null || jsonData.length == 0)
      throw("Notifications Data is empty");
    } catch (err) {
      console.log("Error getting notifications from server.  You might need to refresh the page.  Fetch request returned: " + err);
      const n = {
        "id": crypto.randomUUID(),
        "type": "info",
        "icon": "fa-solid fa-circle-info",
        "title": "Notifications stopped.",
        "messageHTML": "<p>Notifications have stopped.  This happens if the server responded with no data or an error.  Refresh the page to resume notifications.</p>",
        "autohide": true
      }
      stopNotifications();
      stopPageUpdates();
      renderNotification(n, false);
      return;
    }
  notifications = jsonData.notifications.filter(n => {
    // Filter out notifications that have already been processed
    if (processedNotifications.includes(n.id)) {
      return false;
    }

    // Don't show warnings which have expired
    const now = new Date();
    const expiry = new Date(Date.parse(n.expires));
    if (now > expiry)
        return false;
    const valid = new Date(Date.parse(n.posted));

    // Don't show warnings which haven't been posted yet
    if (now < valid)
        return false;
    if (n.url != null && window.location.pathname != n.url)
      return false;
    return true
  });

  // Add newly processed notifications to cache unless they'll be valid soon (1 day)
  notifications.forEach(n => {
    const now = new Date();
    const validSoon = new Date(now.getTime() + (1*24*60*60*1000))
    const posted = new Date(Date.parse(n.posted))
    if(validSoon < posted)
      upcomingNotifications.push(n);
    processedNotifications.push(n.id);
  });
  notifications.forEach(n => {
    renderNotification(n);
  })
}

function processUpcomingNotifications() {
  upcomingNotifications = upcomingNotifications.filter(n => {
    const now = new Date();
    const posted = new Date(Date.parse(n.posted))
    if (now < valid) {
      return true;
    }
    processedNotifications.push(n.id);
    renderNotification(n)
    return false;
  })
}

function getToasts() {
  const toasts = localStorage.getItem("toasts")
  if(toasts === null || toasts.length === 0)
    return [];
  return JSON.parse(toasts)
}

function delToast(toastId) {
  const toasts = getToasts().filter(t => t.id !== toastId);
  localStorage.setItem("toasts", JSON.stringify(toasts));
}

function saveToast(toastId, toastType) {
  const toasts = getToasts();
  // Don't add duplicates
  if( toasts.find((e) => e.id === toastId))
    return
  let expiry = new Date();
  switch(toastType) {
      case "warning":
          expiry.setDate(expiry.getDate() + notificationPreferences.warningHide)
          break;
      case "danger":
          expiry.setDate(expiry.getDate() + notificationPreferences.dangerHide)
          break;
      default:
          expiry.setDate(expiry.getDate() + notificationPreferences.defaultHide)
  }
  toasts.push({id: toastId, type: toastType, expires: expiry});
  localStorage.setItem("toasts", JSON.stringify(toasts));
}

function renderNotification(notification, withCookie=true) {
  const {title, icon, messageHTML, autohide, type, expires, posted, id} = notification;
  const toastEl = document.createElement("div");
  toastEl.id = `toast-${id}`;
  toastEl.className = "toast";
  toastEl.style = "width: 40em !important";
  if(notificationPreferences.forceAutohide)
      toastEl.setAttribute('data-bs-autohide', true);
  else
      toastEl.setAttribute('data-bs-autohide', autohide);
  toastEl.role = "alert";
  toastEl.setAttribute("aria-live", "assertive");
  toastEl.setAttribute("aria-atomic", true);
  typeClasses = "bg-primary text-white";
  switch(type) {
    case "danger":
      typeClasses = "bg-"+ type + " text-white";
      break;
    case "warning":
      typeClasses = "bg-" + type + " text-black";
      break;
    case "success":
      typeClasses = "bg-" + type + " text-white";
      break;
    case "info":
      typeClasses = "bg-" + type + " text-black";
      break;
  }
  toastEl.innerHTML = `
      <div class="toast-header ${typeClasses} h4">
        <strong class="me-auto"><i style="margin-right: 0.75em" class="${icon}"></i> ${title}</strong>
        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
      </div>
      <div class="toast-body">
        ${messageHTML}
      </div>
  `;
  if(withCookie) {
      toastEl.addEventListener('hidden.bs.toast', () => {
          if(document.getElementById(id+"-check") != null) {
              document.getElementById(id+"-check").checked=true;
              e = document.getElementById(id+"-markBtn");
              e.textContent = "Unread";
              e.classList.remove('btn-success');
              e.classList.add("btn-danger");
            }
          saveToast(id, type);
      });
  }
  document.getElementById('toasts').appendChild(toastEl);
  const toast = new bootstrap.Toast(toastEl);
  toast.id = id;
  allToasts.push(toast);
  toast.show();
}

async function saveLegacyToasts(legacyToasts) {
  let response = null;
  let jsonData = null;
  try {
    response = await fetch('/notifications.json', {cache: "no-store"});
    jsonData = await response.json();
    if(jsonData == null || jsonData.length == 0) {
      throw("Notifications Data is empty");
    }

    jsonData.notifications.forEach(n => {
      if(legacyToasts.includes(n.id)) {
        saveToast(n.id, n.type)
      }
    });
  } catch(err) {
    console.log("Something went wrong processing legacy notifications");
  }
}

async function cleanupLegacyToastCookies() {
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  let cookieToasts = []
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf("toast-") == 0) {
      cookieToasts.push(c.split("=")[0].split("toast-")[1]);
    }
  }
  cookieToasts.forEach(c => {
    setCookie("toast-"+c, false, 0);
  });
  await saveLegacyToasts(cookieToasts);
}

async function startNotifications() {
  await cleanupLegacyToastCookies();
  storedToasts = getToasts();
  storedToasts = storedToasts.filter(t => {
    const now = new Date();
    const expiry = new Date(Date.parse(t.expires));
    if (now > expiry)
        return false
    processedNotifications.push(t.id);
    return true;
  });
  localStorage.setItem("toasts", JSON.stringify(storedToasts));

  // Do initial processing of notifications
  processNotifications();
  checkForPageChanges()
  // Then check for new notifications every 30s
  notificationProcess = setInterval(function() {
    processNotifications();
  }, 30000);
  upcomingNotificationsProcess =  setInterval(function() {
    processUpcomingNotifications();
  }, 1000);
  startPageUpdates();
}

function stopNotifications() {
  if(notificationProcess != null)
    clearInterval(notificationProcess);
  if(upcomingNotificationsProcess != null)
    clearInterval(upcomingNotificationsProcess);
  if(checkForUpdatesProcess != null)
    clearInterval(checkForUpdatesProcess);
  allToasts.forEach((t) => {
    t._element.remove();
  });
  upcomingNotifications = [];
  processedNotifications = [];
}

function startPageUpdates() {
  if(notificationPreferences.checkForPageChanges)
    checkForUpdatesProcess = setInterval(function() {
      checkForPageChanges()
    }, 30000);
}

function stopPageUpdates() {
  if(checkForUpdatesProcess != null)
    clearInterval(checkForUpdatesProcess);
}

let notificationPreferences = getCookie("notification-preferences");
let upcomingNotifications = [];
let processedNotifications = [];
let dropCookie = true;
let allToasts = [];
const arrivalTime = new Date();
const parser = new DOMParser();
let notificationProcess = null;
let checkForUpdatesProcess = null
let upcomingNotificationsProcess = null;

$( document ).ready(() => {
  if (notificationPreferences === "") {
      notificationPreferences = {
          "showNotifications": true,
          "forceAutohide": false,
          "checkForPageUpdates": true,
          "dangerHide": 1,
          "warningHide": 7,
          "defaultHide": 365
      };
      setCookie("notification-preferences", JSON.stringify(notificationPreferences), 365);
      const id = crypto.randomUUID();
      const toastEl = document.createElement("div");
      toastEl.id = `toast-${id}`;
      toastEl.className = "toast";
      toastEl.style = "width: 40em !important";
      toastEl.setAttribute('data-bs-autohide', false);
      toastEl.role = "alert";
      toastEl.setAttribute("aria-live", "assertive");
      toastEl.setAttribute("aria-atomic", true);
      typeClasses = "bg-success text-white";
      toastEl.innerHTML = `
            <div class="toast-header bg-success text-white h4">
              <strong class="me-auto"><i style="margin-right: 0.75em" class="fa-brands fa-gitlab"></i> Now with Added Notifications</strong>
            </div>
            <div class="toast-body">
              <p>The Handbook now has notifications.  This allows us to share important updates
              about the site and pages with you as well as providing features such as checking for page updates.
              You can control your notifications from the <a href="/handbook/about/tools/handbook-preferences">
              Handbook Preferences</a> pages.</p>
              <hr>
              <p><strong>Are you happy to receive notifications?</strong></p>

              <p>
                <button type="button" id="yesBtn" class="btn btn-success text-white">Yes</button>
                <button type="button" id="noBtn" class="btn btn-light">No</button>
              </p>
            </div>
        `;
      document.getElementById('toasts').appendChild(toastEl);
      const toast = new bootstrap.Toast(toastEl);
      toast.id = id;
      document.getElementById("noBtn").addEventListener("click", () => {
        notificationPreferences.showNotifications = false;
        setCookie("notification-preferences", JSON.stringify(notificationPreferences), 365);
        stopNotifications()
      });
      document.getElementById("yesBtn").nid = id;
      document.getElementById("yesBtn").addEventListener("click", (e) => {
          t = allToasts.find(t => t.id === e.currentTarget.nid);
          t.hide();
      });
      allToasts.push(toast);
      toast.show();
  } else {
    notificationPreferences = JSON.parse(notificationPreferences);
  }
  //extend the date of the cookie for another year
  setCookie("notification-preferences", JSON.stringify(notificationPreferences), 365);

  if(notificationPreferences.showNotifications) {
    startNotifications()
  }

  // Set gloabal scope items
  globalThis.getCookie = getCookie;
  globalThis.setCookie = setCookie;
  globalThis.getToasts = getToasts;
  globalThis.saveToast = saveToast;
  globalThis.delToast = delToast;
  globalThis.notificationPreferences = notificationPreferences;
  globalThis.renderNotification = renderNotification;
  globalThis.stopNotifications = stopNotifications;
  globalThis.startNotifications = startNotifications;
  globalThis.startPageUpdates = startPageUpdates;
  globalThis.stopPageUpdates = stopPageUpdates;
  globalThis.processedNotifications = processedNotifications;
  globalThis.allToasts = allToasts;
  globalThis.notificationProcess = notificationProcess;
});
